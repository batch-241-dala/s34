const express = require("express");
const app = express();
const port = 3000;
app.use(express.json())
app.use(express.urlencoded({extended: true}));


let message;
let users = [
	{
		"username": "johndoe",
		"password": "johndoe1234"
	},
	{
		"username": "test",
		"password": "testing"
	}
	];


app.get("/home", (request, response) => {
	response.send("Welcome to the home page")
})

app.get("/users", (request, response) => {
	
	response.send(users)
})

app.delete("/delete-user", (request, response) => {

	for (let i = 0; i < users.length; i++) {
		if (request.body.username == users[i].username) {
			message = `User ${request.body.username} has been deleted.`
			break;
		} else {
			message = `User does not exist`
		}
	}
	response.send(message);
})


























app.listen(port, () => console.log(`Server running at port ${port}`));